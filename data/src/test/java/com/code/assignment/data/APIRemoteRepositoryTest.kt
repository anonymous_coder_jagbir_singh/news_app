package com.code.assignment.data

import com.code.assignment.data.callback.APICallbackFactory
import com.code.assignment.data.client.NewsAPI
import com.code.assignment.data.mapper.DataModelMapperFactory
import com.code.assignment.data.model.APIEnclosure
import com.code.assignment.data.model.APIFeedResponse
import com.code.assignment.data.model.APIItem
import com.google.gson.GsonBuilder
import okhttp3.Request
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.spy
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class APIRemoteRepositoryTest : DataTestCase() {

    @Mock
    private lateinit var api: NewsAPI
    private lateinit var dataModelMapperFactory: DataModelMapperFactory
    private lateinit var apiCallbackFactory: APICallbackFactory
    private lateinit var apiRemoteRepository: APIRemoteRepository
    private val gson = GsonBuilder().create()

    private val defaultResponse: APIFeedResponse
        get() {
            return APIFeedResponse(
                items = listOf(
                    APIItem(
                        title = "testTitle",
                        enclosure = APIEnclosure(link = "enclosure"),
                        thumbnail = "link",
                        pubDate = "12-12-1980"
                    )
                )
            )
        }

    override fun setUp() {
        super.setUp()
        dataModelMapperFactory = DataModelMapperFactory()
        apiCallbackFactory = spy(APICallbackFactory())
        apiRemoteRepository = APIRemoteRepository(
            api,
            dataModelMapperFactory,
            apiCallbackFactory,
            gson
        )
    }

    @Test
    fun newsFeedAPITest() {
        val page = 1
        mockDefaultResponse(page, Response.success(defaultResponse))
        apiRemoteRepository.newsFeed().test()
        Mockito.verify(api).newsFeed()
    }

    private fun mockDefaultResponse(page: Int, response: Response<APIFeedResponse>) {
        val mockCall = MockAPICall(response)
        Mockito.`when`(api.newsFeed()).thenReturn(mockCall)
    }

    private fun <T> any(): T = Mockito.any<T>()

    private class MockAPICall<DATA>(
        private val response: Response<DATA>
    ) : Call<DATA> {
        override fun enqueue(callback: Callback<DATA>) {
            execute()
        }

        override fun isExecuted(): Boolean {
            return true
        }

        override fun clone(): Call<DATA> {
            return this
        }

        override fun isCanceled(): Boolean {
            return false
        }

        override fun cancel() {
        }

        override fun execute(): Response<DATA> {
            return response
        }

        override fun request(): Request {
            return Request.Builder().build()
        }
    }

}