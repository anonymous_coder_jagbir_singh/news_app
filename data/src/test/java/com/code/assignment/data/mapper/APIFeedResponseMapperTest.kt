package com.code.assignment.data.mapper

import com.code.assignment.data.carrier.APIResult
import com.code.assignment.data.model.APIEnclosure
import com.code.assignment.data.model.APIFeedResponse
import com.code.assignment.data.model.APIItem
import com.code.assignment.domain.carrier.SResult
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import org.junit.Test

class APIFeedResponseMapperTest {

    @Test
    fun toDomainTest() {
        val mapper = APINewsFeedMapper()
        val data = APIResult.Success(
            APIFeedResponse(
                status = "ok",
                items = listOf(
                    APIItem(
                        title = "testTitle",
                        enclosure = APIEnclosure(link = "enclosure"),
                        thumbnail = "link",
                        pubDate = "12-12-1980"
                    )
                )
            )
        )
        val domain = mapper.toDomain(data)

        assertNotNull(domain)
        val success = domain as? SResult.Success
        assertNotNull(success)
        assertNotNull(success?.data)
        assertEquals(success?.data?.status, data.data.status)
        assertEquals(success?.data?.items?.size, data.data.items?.size)
        assertNotNull(success?.data?.items?.get(0))
        assertEquals(success?.data?.items?.get(0)?.thumbnail, data.data.items?.get(0)?.thumbnail)
        assertEquals(
            success?.data?.items?.get(0)?.title,
            data.data.items?.get(0)?.title
        )
        assertEquals(
            success?.data?.items?.get(0)?.enclosure?.link,
            data.data.items?.get(0)?.enclosure?.link
        )
    }

}