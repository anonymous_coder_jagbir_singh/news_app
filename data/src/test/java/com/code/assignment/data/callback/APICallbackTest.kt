package com.code.assignment.data.callback

import com.code.assignment.data.DataTestCase
import com.code.assignment.data.mapper.DataModelMapperFactory
import com.code.assignment.data.model.APIEnclosure
import com.code.assignment.data.model.APIFeedResponse
import com.code.assignment.data.model.APIItem
import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.usecase.news.model.FeedResponse
import com.google.gson.GsonBuilder
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import okhttp3.Request
import org.junit.Test
import org.mockito.Mockito.spy
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class APICallbackTest : DataTestCase() {

    private val dataModelMapperFactory = DataModelMapperFactory()

    private val defaultResponse: APIFeedResponse
        get() {
            val item = APIItem(
                title = "testTitle",
                enclosure = APIEnclosure(link = "enclosure"),
                thumbnail = "link",
                pubDate = "12-12-1980"
            )
            return APIFeedResponse(
                status = "ok",
                items = listOf(item)
            )
        }

    @Test
    fun onFailureProcessingTest() {
        val errorMsg = "test error"
        val testListener = spy<(SResult<FeedResponse>) -> Unit> {
            val result = it as? SResult.Error
            assertNotNull(result)
            assertEquals(result?.error?.message, errorMsg)
        }
        val callback =
            spy(
                APICallback(
                    APIFeedResponse::class.java,
                    dataModelMapperFactory,
                    testListener,
                    GsonBuilder().create()
                )
            )
        callback.onFailure(
            MockAPICall(Response.success(defaultResponse)),
            IllegalArgumentException(errorMsg)
        )
    }

    @Test
    fun onSuccessProcessingTest() {
        val testListener = spy<(SResult<FeedResponse>) -> Unit> {
            val result = it as? SResult.Success
            assertNotNull(result)
            assertEquals(result?.data?.status, defaultResponse.status)
            assertEquals(result?.data?.items?.size, defaultResponse.items?.size)
            assertEquals(
                result?.data?.items?.get(0)?.title,
                defaultResponse.items?.get(0)?.title
            )
            assertEquals(
                result?.data?.items?.get(0)?.thumbnail,
                defaultResponse.items?.get(0)?.thumbnail
            )
        }
        val callback = spy(
            APICallback(
                APIFeedResponse::class.java,
                dataModelMapperFactory,
                testListener,
                GsonBuilder().create()
            )
        )
        callback.onResponse(
            MockAPICall(Response.success(defaultResponse)),
            Response.success(defaultResponse)
        )
    }

    private class MockAPICall(
        private val response: Response<APIFeedResponse>
    ) : Call<APIFeedResponse> {
        override fun enqueue(callback: Callback<APIFeedResponse>) {
            execute()
        }

        override fun isExecuted(): Boolean {
            return true
        }

        override fun clone(): Call<APIFeedResponse> {
            return this
        }

        override fun isCanceled(): Boolean {
            return false
        }

        override fun cancel() {
        }

        override fun execute(): Response<APIFeedResponse> {
            return response
        }

        override fun request(): Request {
            return Request.Builder().build()
        }
    }
}