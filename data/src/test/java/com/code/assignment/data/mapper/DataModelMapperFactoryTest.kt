package com.code.assignment.data.mapper

import com.code.assignment.data.model.APIFeed
import com.code.assignment.data.model.APIFeedResponse
import com.code.assignment.domain.usecase.news.model.Feed
import com.code.assignment.domain.usecase.news.model.FeedResponse
import junit.framework.TestCase.assertEquals
import org.junit.Test

class DataModelMapperFactoryTest {

    @Test
    fun newsFeedMapperTest() {
        assertEquals(
            "The mapper instance returned is not of type: "
                    + APINewsFeedMapper::class.java,
            DataModelMapperFactory().create<APIFeedResponse, FeedResponse>(
                APIFeedResponse::class.java
            ).javaClass,
            APINewsFeedMapper::class.java
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun noMapperFoundTest() {
        DataModelMapperFactory().create<APIFeed, Feed>(APIFeed::class.java).javaClass
    }

}