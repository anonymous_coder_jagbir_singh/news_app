package com.code.assignment.data

import com.code.assignment.data.client.NewsAPI
import io.reactivex.Observable
import junit.framework.TestCase.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Test cases for testing [NewsAPI] methods from
 * an integration (with server) point of view.
 *
 * PLEASE NOTE:
 * These test cases shall fail in case there is no
 * connectivity with the server.
 *
 */
class NewsAPIIntegrationTest {

    private lateinit var api: NewsAPI

    @Before
    @Throws(Exception::class)
    fun setUp() {

        val BASE_URL = "https://api.rss2json.com/"

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient
            .Builder()
            .addInterceptor(interceptor)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

        api = retrofit.create(NewsAPI::class.java)
    }

    @Test
    fun newsFeedCallTest() {
        val testObserver = Observable.just(api.newsFeed().execute()).test()

        testObserver.assertComplete()
        testObserver.assertNoErrors()

        val values = testObserver.values()

        assertEquals(1, values.size)
        assertNotNull(values[0])
        assertNotNull(values[0].body())
        assertTrue(values[0].body()?.status.orEmpty() == "ok")
        if (values[0].body()?.items?.isNotEmpty() == true) {
            assertNotNull(values[0].body()?.items?.get(0)?.title?.isNotEmpty())
            assertNotNull(values[0].body()?.items?.get(0)?.pubDate?.isNotEmpty())
            assertNotNull(values[0].body()?.items?.get(0)?.thumbnail?.isNotEmpty())
        }
    }
}