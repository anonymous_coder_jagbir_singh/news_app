package com.code.assignment.data

import org.junit.Before
import org.mockito.MockitoAnnotations

open class DataTestCase {

    @Before
    @Throws(Exception::class)
    open fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

}