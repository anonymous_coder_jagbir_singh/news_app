package com.code.assignment.data

import com.code.assignment.data.callback.APICallbackFactory
import com.code.assignment.data.client.NewsAPI
import com.code.assignment.data.mapper.DataModelMapperFactory
import com.code.assignment.data.model.APIFeedResponse
import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.repository.RemoteRepository
import com.code.assignment.domain.usecase.news.model.FeedResponse
import com.google.gson.Gson
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Implementation of [RemoteRepository] and helps fetch data from the remote sever
 * via [NewsAPI].
 * The data is then mapped into DOMAIN layer models via mappers that are provided through [DataModelMapperFactory]
 */
class APIRemoteRepository @Inject constructor(
    private val api: NewsAPI,
    private val dataMapperFactory: DataModelMapperFactory,
    private val apiCallbackFactory: APICallbackFactory,
    private val gson: Gson
) : RemoteRepository {

    override fun newsFeed(): Observable<SResult<FeedResponse>> {
        return Observable.create { emitter ->
            api.newsFeed().enqueue(
                apiCallbackFactory.provideAPICallback<APIFeedResponse, FeedResponse>(
                    dataModelType = APIFeedResponse::class.java,
                    dataModelMapperFactory = dataMapperFactory,
                    gson = gson
                ) {
                    emitter.onNext(it)
                    emitter.onComplete()
                }
            )
        }
    }
}