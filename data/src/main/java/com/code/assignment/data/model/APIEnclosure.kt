package com.code.assignment.data.model

import com.google.gson.annotations.SerializedName

data class APIEnclosure(
    @SerializedName("link")
    val link: String? = null
    /*
        Other fields described by the server API contract,
        shall be added as required.
    */
)