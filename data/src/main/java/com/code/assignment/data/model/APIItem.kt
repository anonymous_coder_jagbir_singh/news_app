package com.code.assignment.data.model

import com.google.gson.annotations.SerializedName

data class APIItem(
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("pubDate")
    val pubDate: String? = null,
    @SerializedName("enclosure")
    val enclosure: APIEnclosure? = null,
    @SerializedName("thumbnail")
    val thumbnail: String? = null,
    @SerializedName("content")
    val content: String? = null
    /*
        Other fields described by the server API contract,
        shall be added as required.
     */
)