package com.code.assignment.data.mapper

import com.code.assignment.data.carrier.APIResult
import com.code.assignment.data.model.APIFeedResponse
import com.code.assignment.domain.carrier.SResult
import javax.inject.Inject

/**
 * Factory which provides the respective implementation
 * of [APIMapper] based on the DOMAIN model type to be mapped
 * from the DATA layer model
 */
class DataModelMapperFactory @Inject constructor() {

    @Suppress("UNCHECKED_CAST")
    @Throws(IllegalArgumentException::class)
    fun <DATA, DOMAIN> create(dataModelType: Class<DATA>): APIMapper<APIResult<DATA>, SResult<DOMAIN>> {

        val mapper: APIMapper<*, *> = when (dataModelType) {

            APIFeedResponse::class.java -> APINewsFeedMapper()

            else -> throw IllegalArgumentException("API Data model class type unknown: $dataModelType")
        }

        return mapper as APIMapper<APIResult<DATA>, SResult<DOMAIN>>
    }

}