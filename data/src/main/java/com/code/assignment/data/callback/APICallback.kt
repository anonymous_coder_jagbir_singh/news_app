package com.code.assignment.data.callback

import com.code.assignment.data.carrier.APIError
import com.code.assignment.data.carrier.APIResult
import com.code.assignment.data.mapper.DataModelMapperFactory
import com.code.assignment.data.model.APIErrorBody
import com.code.assignment.domain.carrier.SResult
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class APICallback<DATA, DOMAIN>(
    private val dataModelType: Class<DATA>,
    private val dataModelMapperFactory: DataModelMapperFactory,
    private val listener: (SResult<DOMAIN>) -> Unit,
    private val gson: Gson
) : Callback<DATA> {

    override fun onFailure(call: Call<DATA>, t: Throwable) {
        processResponse(throwable = t, dataModelType = dataModelType)
    }

    override fun onResponse(call: Call<DATA>, response: Response<DATA>) {
        processResponse(response = response, dataModelType = dataModelType)
    }

    private fun processResponse(
        response: Response<DATA>? = null,
        throwable: Throwable? = null,
        dataModelType: Class<DATA>
    ) {
        val item = if (response != null) {
            val apiData = if (response.isSuccessful) {
                response.body()?.let {
                    APIResult.Success<DATA>(it)
                } ?: APIResult.Error(APIError(""))
            } else {
                APIResult.Error(APIError(parseError(response.errorBody()?.string().orEmpty())))
            }
            dataModelMapperFactory.create<DATA, DOMAIN>(dataModelType)
                .toDomain(
                    apiData
                )
        } else {
            dataModelMapperFactory.create<DATA, DOMAIN>(dataModelType)
                .toDomain(
                    APIResult.Error(APIError(throwable?.message.orEmpty()))
                )
        }
        listener.invoke(item)
    }

    private fun parseError(jsonStr: String): String {
        return try {
            val error = gson.fromJson(jsonStr, APIErrorBody::class.java)
            error.detail
        } catch (e: JsonSyntaxException) {
            "An unknown error has occurred. Please try again later."
        }
    }
}