package com.code.assignment.data.model

import com.google.gson.annotations.SerializedName

data class APIFeedResponse(
    @SerializedName("status")
    var status: String? = null,
    @SerializedName("feed")
    var feed: APIFeed? = null,
    @SerializedName("items")
    var items: List<APIItem>? = null
)