package com.code.assignment.data.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    /**
     * Method to convert a string into a [Date] instance,
     * based on the formatting provided.
     *
     * Default Formatting: "yyyy-MM-dd HH:mm:ss"
     *
     * @param dateString the string representation of the date
     * @param dateFormat the format in which the date string is formatted
     */
    fun toDate(dateString: String?, dateFormat: String = "yyyy-MM-dd HH:mm:ss"): Date {
        return if (dateString != null) {
            val format = SimpleDateFormat(dateFormat, Locale.getDefault())

            try {
                format.parse(dateString)
            } catch (e: ParseException) {
                //Default to current time
                Date()
            }
        } else {
            Date()
        }
    }

}