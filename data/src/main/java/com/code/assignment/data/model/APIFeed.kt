package com.code.assignment.data.model

import com.google.gson.annotations.SerializedName

data class APIFeed(
    @SerializedName("title")
    val title: String? = null
    /*
        Other fields described by the server API contract,
        shall be added as required.
     */
)
