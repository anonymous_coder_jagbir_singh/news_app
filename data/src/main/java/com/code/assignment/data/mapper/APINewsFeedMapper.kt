package com.code.assignment.data.mapper

import com.code.assignment.data.data.mapper.APIBaseMapper
import com.code.assignment.data.model.APIFeedResponse
import com.code.assignment.data.util.DateUtils
import com.code.assignment.domain.usecase.news.model.Enclosure
import com.code.assignment.domain.usecase.news.model.Feed
import com.code.assignment.domain.usecase.news.model.FeedResponse
import com.code.assignment.domain.usecase.news.model.Item

class APINewsFeedMapper : APIBaseMapper<APIFeedResponse, FeedResponse>() {

    override fun modelToDomain(dataModel: APIFeedResponse): FeedResponse {
        val items = mutableListOf<Item>()
        for (apiItem in dataModel.items.orEmpty()) {
            val item = Item(
                apiItem.title.orEmpty(),
                DateUtils.toDate(apiItem.pubDate),
                Enclosure(
                    apiItem.enclosure?.link?.split("?")?.getOrNull(0).orEmpty()
                ),
                apiItem.thumbnail?.split("?")?.getOrNull(0).orEmpty(),
                apiItem.content.orEmpty()
            )

            items.add(item)
        }

        return FeedResponse(
            dataModel.status.orEmpty(),
            Feed(dataModel.feed?.title.orEmpty()),
            items
        )
    }
}