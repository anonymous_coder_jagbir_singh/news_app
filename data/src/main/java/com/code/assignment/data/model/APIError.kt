package com.code.assignment.data.model

data class APIError(var message: String? = null, var code: Int? = 0)