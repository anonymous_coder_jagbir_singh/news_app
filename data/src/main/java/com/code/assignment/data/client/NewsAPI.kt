package com.code.assignment.data.client

import com.code.assignment.data.model.APIFeedResponse
import retrofit2.Call
import retrofit2.http.GET

/**
 * The Retrofit client for respective API calls.
 *
 */
interface NewsAPI {

    @GET("/v1/api.json?rss_url=http://www.abc.net.au/news/feed/51120/rss.xml")
    fun newsFeed(): Call<APIFeedResponse>

}