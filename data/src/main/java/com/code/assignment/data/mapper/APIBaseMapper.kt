package com.code.assignment.data.data.mapper

import com.code.assignment.data.carrier.APIResult
import com.code.assignment.data.mapper.APIMapper
import com.code.assignment.domain.carrier.SError
import com.code.assignment.domain.carrier.SResult

/**
 *
 * Base API Mapper implementation to handle all common mappings
 * to DOMAIN layer.
 * And also to handle common error scenarios.
 *
 * Based on the current set of requirements,
 * it is assumed that all models shall conform to APIResult<DATA_MODEL>.
 *
 * [APIMapper] can be directly implemented for models which do not conform to the above.
 */
abstract class APIBaseMapper<DATA_MODEL, DOMAIN_MODEL> :
    APIMapper<APIResult<DATA_MODEL>, SResult<DOMAIN_MODEL>> {

    override fun toDomain(data: APIResult<DATA_MODEL>): SResult<DOMAIN_MODEL> {
        return when (data) {
            is APIResult.Success -> {
                SResult.Success(modelToDomain(data.data))
            }
            is APIResult.Error -> {
                SResult.Error(error = SError(data.error.message))
            }
        }
    }

    protected abstract fun modelToDomain(dataModel: DATA_MODEL): DOMAIN_MODEL
}