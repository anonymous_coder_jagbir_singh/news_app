package com.code.assignment.data.carrier

/**
 * The base class to contain any errors encountered
 * during the functioning of the use-cases.
 */
data class APIError(var message: String = "")