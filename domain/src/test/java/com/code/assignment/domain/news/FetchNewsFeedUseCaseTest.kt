package com.code.assignment.domain.usecase.news

import com.code.assignment.domain.DomainTestCase
import com.code.assignment.domain.carrier.SError
import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.repository.RemoteRepository
import com.code.assignment.domain.thread.ObserverThread
import com.code.assignment.domain.usecase.news.model.Enclosure
import com.code.assignment.domain.usecase.news.model.FeedResponse
import com.code.assignment.domain.usecase.news.model.Item
import io.reactivex.Observable
import junit.framework.TestCase
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import java.util.*
import java.util.concurrent.Executor

class FetchNewsFeedUseCaseTest : DomainTestCase() {

    @Mock
    lateinit var repository: RemoteRepository

    @Mock
    lateinit var observerThread: ObserverThread

    @Mock
    lateinit var executor: Executor

    private lateinit var feedUseCase: FetchNewsFeedUseCase

    private val defaultResponse: SResult<FeedResponse>
        get() {
            val item = Item(
                title = "testTitle",
                enclosure = Enclosure(link = "enclosure"),
                thumbnail = "link",
                pubDate = Date()
            )
            val model = FeedResponse(
                items = listOf(item)
            )
            return SResult.Success(model)
        }

    private val errorDataResponse: SResult<FeedResponse>
        get() {
            return SResult.Error(error = SError("something went wrong"))
        }

    override fun setUp() {
        super.setUp()
        feedUseCase =
            FetchNewsFeedUseCase(
                observerThread,
                executor,
                repository
            )
    }

    @Test
    fun `WHEN use-case is subscribed THEN it gets completed successfully`() {
        Mockito.`when`(repository.newsFeed()).thenReturn(Observable.just(defaultResponse))
        val observer = feedUseCase.constructObservable(Unit).test()
        observer.assertComplete()
    }

    @Test
    fun `WHEN use-case is subscribed THEN correct repository method is called`() {
        Mockito.`when`(repository.newsFeed()).thenReturn(Observable.just(defaultResponse))
        feedUseCase.constructObservable(Unit).test()
        Mockito.verify(repository, Mockito.times(1)).newsFeed()
    }

    @Test
    fun `GIVEN use-case is subscribed WHEN repository is called THEN correct data is returned`() {
        val response = defaultResponse
        Mockito.`when`(repository.newsFeed()).thenReturn(Observable.just(response))
        val observer = feedUseCase.constructObservable(Unit).test()
        observer.assertValue(response)
    }

    @Test
    fun `GIVEN use-case is subscribed WHEN repository returns error THEN error is returned`() {
        Mockito.`when`(repository.newsFeed()).thenReturn(Observable.just(errorDataResponse))
        val observer = feedUseCase.constructObservable(Unit).test()
        val result = observer.values()[0]
        TestCase.assertTrue(result is SResult.Error)
        TestCase.assertTrue(result != null)
    }

}