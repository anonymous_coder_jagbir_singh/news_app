package com.code.assignment.domain.usecase.news

import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.repository.RemoteRepository
import com.code.assignment.domain.thread.ObserverThread
import com.code.assignment.domain.usecase.ObservableUseCase
import com.code.assignment.domain.usecase.news.model.FeedResponse
import io.reactivex.Observable
import java.util.concurrent.Executor
import javax.inject.Inject

/**
 * The use-case to fetch the news feed from the remote API.
 */
class FetchNewsFeedUseCase @Inject constructor(
    observerThread: ObserverThread,
    threadExecutor: Executor,
    private val remoteRepository: RemoteRepository
) : ObservableUseCase<SResult<FeedResponse>, Unit>(observerThread, threadExecutor) {

    override fun constructObservable(params: Unit?): Observable<SResult<FeedResponse>> {
        return remoteRepository.newsFeed()
    }
}