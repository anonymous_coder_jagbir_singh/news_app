package com.code.assignment.domain.usecase.news.model

data class Enclosure(
    val link: String = ""
    /*
        Other fields described by the server API contract,
        shall be added as required.
    */
)