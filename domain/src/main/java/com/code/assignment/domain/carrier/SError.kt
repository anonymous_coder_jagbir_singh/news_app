package com.code.assignment.domain.carrier

/**
 * The base class to contain any errors encountered
 * during the functioning of the use-cases.
 */
data class SError(var message: String = "")