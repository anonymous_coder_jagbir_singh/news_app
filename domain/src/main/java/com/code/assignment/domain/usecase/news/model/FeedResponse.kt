package com.code.assignment.domain.usecase.news.model

//TODO("The status can be converted to an ENUM")
data class FeedResponse(
    val status: String = "ok",
    val feed: Feed = Feed(),
    val items: List<Item> = emptyList()
) {
    companion object {
        const val STATUS_OK = "ok"
    }
}