package com.code.assignment.domain.repository

import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.usecase.news.model.FeedResponse
import io.reactivex.Observable

/**
 * The interface which defined the interactions which REMOTE world.
 *
 */
interface RemoteRepository {

    /**
     * fetch news content
     *
     * @return Observable<SResult<FeedResponse>>
     */
    fun newsFeed(): Observable<SResult<FeedResponse>>

}