package com.code.assignment.domain.usecase.news.model

data class Feed(
    val title: String = ""
    /*
        Other fields described by the server API contract,
        shall be added as required.
     */
)
