package com.code.assignment.domain.usecase.news.model

import java.util.*

data class Item(
    val title: String = "",
    val pubDate: Date = Date(),
    val enclosure: Enclosure = Enclosure(),
    val thumbnail: String = "",
    val content: String = ""
    /*
        Other fields described by the server API contract,
        shall be added as required.
     */
)