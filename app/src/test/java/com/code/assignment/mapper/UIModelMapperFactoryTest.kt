package com.code.assignment.mapper

import com.code.assignment.UITestCase
import com.code.assignment.domain.usecase.news.model.FeedResponse
import com.code.assignment.domain.usecase.news.model.Item
import com.code.assignment.ui.mapper.UIModelMapperFactory
import com.code.assignment.ui.screen.feed.mapper.UINewsFeedMapper
import com.code.assignment.ui.screen.feed.model.UINewsFeedModel
import com.code.assignment.ui.uicomponents.model.UIItem
import junit.framework.TestCase
import org.junit.Test

class UIModelMapperFactoryTest : UITestCase() {

    @Test
    fun uiNEwsFeedMapperTest() {
        TestCase.assertEquals(
            "The mapper instance returned is not of type: "
                    + UINewsFeedMapper::class.java,
            UIModelMapperFactory().create<UINewsFeedModel, FeedResponse>(
                UINewsFeedModel::class.java
            ).javaClass,
            UINewsFeedMapper::class.java
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun noMapperFoundTest() {
        UIModelMapperFactory().create<UIItem, Item>(UIItem::class.java).javaClass
    }

}