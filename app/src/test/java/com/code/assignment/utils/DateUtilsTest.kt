package com.code.assignment.utils

import com.code.assignment.ui.utils.`toStringMMM-d-YYYY-HH-mm-a`
import org.junit.Assert.assertEquals
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class DateUtilsTest {

    @Test
    fun toFormattedDateTest() {
        val original = "May 11, 2021 04:53 am"
        val formatter = SimpleDateFormat("MMM d, yyyy hh:mm a", Locale.getDefault())
        val date = formatter.parse(original)
        assertEquals(original, date?.`toStringMMM-d-YYYY-HH-mm-a`())
    }

}