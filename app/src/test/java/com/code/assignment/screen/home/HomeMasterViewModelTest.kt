package com.code.assignment.screen.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.code.assignment.UITestCase
import com.code.assignment.ui.screen.feed.model.UINewsItem
import com.code.assignment.ui.screen.home.FragmentType
import com.code.assignment.ui.screen.home.HomeMasterViewModel
import com.code.assignment.ui.uicomponents.model.UIError
import com.nhaarman.mockitokotlin2.argumentCaptor
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import java.util.*

class HomeMasterViewModelTest : UITestCase() {

    private lateinit var viewModel: HomeMasterViewModel

    @Mock
    private lateinit var progressBarObserver: Observer<Boolean?>

    @Mock
    private lateinit var errorToastObserver: Observer<UIError?>

    @Mock
    private lateinit var popBackStackObserver: Observer<Unit?>

    @get:Rule
    val rule = InstantTaskExecutorRule()

    override fun setUp() {
        super.setUp()

        viewModel = HomeMasterViewModel()

        viewModel.progressVisibilityLiveData.observeForever(progressBarObserver)
        viewModel.errorToastLiveData.observeForever(errorToastObserver)
        viewModel.popBackStackLiveData.observeForever(popBackStackObserver)
    }

    @Test
    fun progressBarTest() {
        viewModel.toggleBusyIndicator(true)
        Mockito.verify(progressBarObserver).onChanged(true)
    }

    @Test
    fun errorToastTest() {
        val captor = argumentCaptor<UIError>()
        val errorMsg = "error msg"
        val uiError = UIError(errorMsg)
        viewModel.showToast(uiError)
        Mockito.verify(errorToastObserver).onChanged(captor.capture())
        assertEquals(errorMsg, captor.firstValue.message)
    }

    @Test
    fun initTest() {
        viewModel.init()

        assertEquals(
            viewModel.fragmentMovementLiveData.value?.masterListenerClass,
            HomeMasterViewModel::class.java
        )
        assertEquals(viewModel.fragmentMovementLiveData.value?.fragmentType, FragmentType.NEWS_FEED)
        assertEquals(viewModel.fragmentMovementLiveData.value?.addToBackStack, false)
    }

    @Test
    fun newsItemDetailsRequestedTest() {
        val uiItem = UINewsItem(
            "123",
            "name",
            Date(),
            "/image_url.jpg",
            123
        )
        viewModel.onNewsDetailsRequested(uiItem)

        assertEquals(uiItem.id, viewModel.getNewsItemDetails()?.id)
        assertEquals(
            viewModel.fragmentMovementLiveData.value?.masterListenerClass,
            HomeMasterViewModel::class.java
        )
        assertEquals(
            viewModel.fragmentMovementLiveData.value?.fragmentType,
            FragmentType.ITEM_DETAILS
        )
        assertEquals(viewModel.fragmentMovementLiveData.value?.addToBackStack, true)
        assertNull(viewModel.fragmentMovementLiveData.value?.params)
    }
}