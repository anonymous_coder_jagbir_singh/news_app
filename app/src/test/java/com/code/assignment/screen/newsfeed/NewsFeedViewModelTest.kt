package com.code.assignment.screen.newsfeed

import com.code.assignment.UITestCase
import com.code.assignment.domain.usecase.news.FetchNewsFeedUseCase
import com.code.assignment.ui.mapper.UIModelMapperFactory
import com.code.assignment.ui.screen.feed.NewsFeedViewModel
import com.code.assignment.ui.screen.feed.model.UINewsItem
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.times
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.*

class NewsFeedViewModelTest : UITestCase() {

    private lateinit var viewModel: NewsFeedViewModel

    @Mock
    private lateinit var masterListener: NewsFeedViewModel.Listener

    @Mock
    private lateinit var usecase: FetchNewsFeedUseCase

    override fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = Mockito.spy(
            NewsFeedViewModel(
                usecase,
                uiModelMapperFactory = UIModelMapperFactory()
            )
        )
        viewModel.setMasterViewModelCommListener(masterListener)
        viewModel.fetchNewsFeed()
    }

    @Test
    fun onLoadNewsFeedTest() {
        Mockito.verify(usecase).execute(any(), eq(null))
        Mockito.verify(masterListener, times(1)).toggleBusyIndicator(true)
    }

    @Test
    fun onItemClickedTest() {
        val item = UINewsItem(
            "123",
            "name",
            Date(),
            "/image_url.jpg",
            123
        )
        viewModel.onItemClicked(item)
        Mockito.verify(masterListener).onNewsDetailsRequested(item)
    }
}