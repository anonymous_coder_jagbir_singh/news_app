package com.code.assignment.ui.screen.feed.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import au.com.abc.news.R
import au.com.abc.news.databinding.ItemNewsBinding
import au.com.abc.news.databinding.ItemNewsHeroBinding
import com.code.assignment.ui.screen.feed.model.UIHeroNewsItem
import com.code.assignment.ui.screen.feed.viewholder.UIHeroNewsViewHolder
import com.code.assignment.ui.screen.feed.viewholder.UINewsViewHolder
import com.code.assignment.ui.uicomponent.listener.OnItemViewClickListener
import com.code.assignment.ui.uicomponents.adapter.SAdapter
import com.code.assignment.ui.uicomponents.model.UIItem
import com.code.assignment.ui.uicomponents.viewholder.SItemViewHolder

class NewsFeedAdapter(
    listener: OnItemViewClickListener<UIItem>
) : SAdapter<OnItemViewClickListener<UIItem>>(listener) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SItemViewHolder<*, UIItem, OnItemViewClickListener<UIItem>> {
        val viewHolder = when (viewType) {

            UIHeroNewsItem.ITEM_VIEW_TYPE -> {
                val binding =
                    DataBindingUtil.inflate<ItemNewsHeroBinding>(
                        LayoutInflater.from(parent.context),
                        R.layout.item_news_hero,
                        parent,
                        false
                    )
                UIHeroNewsViewHolder(
                    binding,
                    listener
                )
            }

            else -> {
                //UIHeaderItem is considered default here
                val binding =
                    DataBindingUtil.inflate<ItemNewsBinding>(
                        LayoutInflater.from(parent.context),
                        R.layout.item_news,
                        parent,
                        false
                    )
                UINewsViewHolder(
                    binding,
                    listener
                )
            }
        }
        return viewHolder as SItemViewHolder<*, UIItem, OnItemViewClickListener<UIItem>>
    }
}