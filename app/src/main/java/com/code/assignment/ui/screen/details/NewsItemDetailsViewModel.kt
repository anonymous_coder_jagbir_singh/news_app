package com.code.assignment.ui.screen.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.code.assignment.ui.child.BaseWorkflowViewModel
import com.code.assignment.ui.screen.feed.model.UINewsItem
import javax.inject.Inject

class NewsItemDetailsViewModel @Inject constructor() :
    BaseWorkflowViewModel<NewsItemDetailsViewModel.Listener>() {

    private val _modelLiveData: MutableLiveData<UINewsItem?> = MutableLiveData()
    val modelLiveData = _modelLiveData as LiveData<UINewsItem?>

    override fun onMasterCommListenerSet() {
        _modelLiveData.postValue(masterListener?.getNewsItemDetails())
    }

    /**
     * The interface which enables communication with the
     * master/controller
     */
    interface Listener {

        fun getNewsItemDetails(): UINewsItem?

    }
}