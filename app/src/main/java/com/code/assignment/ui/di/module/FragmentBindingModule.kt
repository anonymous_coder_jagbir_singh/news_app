package com.code.assignment.ui.di.module

import com.code.assignment.ui.screen.details.NewsItemDetailsFragment
import com.code.assignment.ui.screen.feed.NewsFeedFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBindingModule {

    @ContributesAndroidInjector
    internal abstract fun bindNewsFeedFragment(): NewsFeedFragment

    @ContributesAndroidInjector
    internal abstract fun bindNewsItemDetailsFragment(): NewsItemDetailsFragment

}