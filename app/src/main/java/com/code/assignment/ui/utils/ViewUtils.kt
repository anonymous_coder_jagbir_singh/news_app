package com.code.assignment.ui.utils

import android.content.res.Resources
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorInt

fun View.setMarginTop(marginTop: Int) {
    val params = this.layoutParams as ViewGroup.MarginLayoutParams
    params.setMargins(params.leftMargin, marginTop, params.rightMargin, params.bottomMargin)
    this.layoutParams = params
}

fun Resources.statusBarHeight(): Int {
    val idStatusBarHeight = this.getIdentifier("status_bar_height", "dimen", "android")
    return if (idStatusBarHeight > 0) {
        this.getDimensionPixelSize(idStatusBarHeight)
    } else {
        0
    }
}

fun TextView.setColoredText(text: String, coloredSubText: String, @ColorInt color: Int) {
    val spannable = TextFormatUtils.getColoredText(text, color, coloredSubText)
    this.text = spannable
}