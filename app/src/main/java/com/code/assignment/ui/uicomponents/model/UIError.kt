package com.code.assignment.ui.uicomponents.model

/**
 * The base error model to error messages
 * that need to be shown on the UI Layer
 */
open class UIError(val message: String? = null)