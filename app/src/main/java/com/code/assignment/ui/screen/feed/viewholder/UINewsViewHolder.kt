package com.code.assignment.ui.screen.feed.viewholder

import androidx.core.content.ContextCompat
import au.com.abc.news.R
import au.com.abc.news.databinding.ItemNewsBinding
import com.code.assignment.ui.screen.feed.model.UINewsItem
import com.code.assignment.ui.uicomponent.listener.OnItemViewClickListener
import com.code.assignment.ui.uicomponents.model.UIItem
import com.code.assignment.ui.uicomponents.viewholder.SItemViewHolder
import com.code.assignment.ui.utils.ImageUtils
import com.code.assignment.ui.utils.`toSpannableMMM-d-YYYY-HH-mm-a`

class UINewsViewHolder(
    binding: ItemNewsBinding,
    listener: OnItemViewClickListener<UIItem>
) : SItemViewHolder<ItemNewsBinding, UINewsItem, OnItemViewClickListener<UIItem>>(
    binding,
    listener
) {

    override fun bind(item: UINewsItem, index: Int) {
        setOnClickListener(item)
        item.apply {
            binding.date.text = date.`toSpannableMMM-d-YYYY-HH-mm-a`(
                ContextCompat.getColor(binding.date.context, R.color.colorDarkGrey2)
            )
            binding.title.text = title
            ImageUtils.loadImage(imageUrl, binding.image)
        }
    }
}