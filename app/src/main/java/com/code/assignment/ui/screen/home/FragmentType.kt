package com.code.assignment.ui.screen.home

/**
 * All types of Fragments which are under the
 * purview of [HomeActivity]
 */
enum class FragmentType {

    NEWS_FEED,

    ITEM_DETAILS

}