package com.code.assignment.ui.utils

import android.view.View
import android.widget.ImageView
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import au.com.abc.news.R
import com.bumptech.glide.Glide

object ImageUtils {

    fun loadImage(
        url: String,
        imageView: ImageView?,
        @ColorInt
        fallbackColor: Int? = null
    ) {
        imageView?.apply {
            if (url.isNotEmpty()) {
                val path = url
                visibility = View.VISIBLE
                Glide.with(context)
                    .load(path)
                    .placeholder(R.color.colorPrimary)
                    .error(R.color.colorAccent)
                    .centerCrop()
                    .into(imageView)
            } else {
                setBackgroundColor(
                    ContextCompat.getColor(
                        this.context,
                        fallbackColor ?: R.color.colorLightGrey
                    )
                )
            }
        }
    }
}