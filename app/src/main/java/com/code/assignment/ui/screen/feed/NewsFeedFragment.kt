package com.code.assignment.ui.screen.feed


import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import au.com.abc.news.R
import au.com.abc.news.databinding.FragmentNewsFeedBinding
import com.code.assignment.ui.child.BaseWorkflowFragment
import com.code.assignment.ui.screen.feed.adapter.NewsFeedAdapter
import com.code.assignment.ui.uicomponent.listener.OnItemViewClickListener
import com.code.assignment.ui.uicomponents.model.UIItem

class NewsFeedFragment :
    BaseWorkflowFragment<NewsFeedViewModel.Listener, NewsFeedViewModel, FragmentNewsFeedBinding>() {

    companion object {

        fun <MASTER_LISTENER> newInstance(masterListenerClass: Class<MASTER_LISTENER>): NewsFeedFragment where MASTER_LISTENER : ViewModel, MASTER_LISTENER : NewsFeedViewModel.Listener {

            val fragment = NewsFeedFragment()

            val args = Bundle()
            args.putSerializable(ARG_MASTER_COMM_VIEW_MODEL, masterListenerClass)
            fragment.arguments = args

            return fragment
        }
    }

    private lateinit var adapter: NewsFeedAdapter

    override fun getViewModelClassType(): Class<NewsFeedViewModel> {
        return NewsFeedViewModel::class.java
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initLiveData()

        setupUI()

        if (savedInstanceState == null && isNewCreation) {
            viewModel.fetchNewsFeed()
        }
    }

    private fun initLiveData() {
        viewModel.modelLiveData.observe(this, {
            it?.apply {
                binding.swipeContainer.isRefreshing = false
                binding.toolbar.toolbar.title = this.title
                adapter.setData(this.items)
            }
        })
    }

    private fun setupUI() {
        binding.recyclerView.layoutManager = LinearLayoutManager(context)

        adapter = NewsFeedAdapter(
            object : OnItemViewClickListener<UIItem> {
                override fun onItemClicked(item: UIItem) {
                    viewModel.onItemClicked(item)
                }
            })
        binding.recyclerView.adapter = adapter

        binding.swipeContainer.setOnRefreshListener {
            binding.swipeContainer.isRefreshing = true
            viewModel.onRefreshRequested()
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_news_feed
    }
}
