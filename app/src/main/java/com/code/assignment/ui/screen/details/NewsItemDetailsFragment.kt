package com.code.assignment.ui.screen.details


import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import au.com.abc.news.R
import au.com.abc.news.databinding.FragmentNewsItemDetailsBinding
import com.code.assignment.ui.child.BaseWorkflowFragment
import com.code.assignment.ui.utils.ImageUtils
import com.code.assignment.ui.utils.`toSpannableMMM-d-YYYY-HH-mm-a`

class NewsItemDetailsFragment :
    BaseWorkflowFragment<NewsItemDetailsViewModel.Listener, NewsItemDetailsViewModel, FragmentNewsItemDetailsBinding>() {

    companion object {

        fun <MASTER_LISTENER> newInstance(masterListenerClass: Class<MASTER_LISTENER>): NewsItemDetailsFragment where MASTER_LISTENER : ViewModel, MASTER_LISTENER : NewsItemDetailsViewModel.Listener {

            val fragment = NewsItemDetailsFragment()

            val args = Bundle()
            args.putSerializable(ARG_MASTER_COMM_VIEW_MODEL, masterListenerClass)
            fragment.arguments = args

            return fragment
        }
    }

    override fun getViewModelClassType(): Class<NewsItemDetailsViewModel> {
        return NewsItemDetailsViewModel::class.java
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initLiveData()
    }

    private fun initLiveData() {
        viewModel.modelLiveData.observe(this, Observer {
            it?.apply {
                binding.date.text = date.`toSpannableMMM-d-YYYY-HH-mm-a`(
                    ContextCompat.getColor(binding.date.context, R.color.colorDarkGrey2)
                )
                binding.title.text = title
                ImageUtils.loadImage(imageUrl, binding.image)
                binding.content.text = content
            }
        })
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_news_item_details
    }
}
