package com.code.assignment.ui.screen.feed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.usecase.news.FetchNewsFeedUseCase
import com.code.assignment.domain.usecase.news.model.FeedResponse
import com.code.assignment.ui.child.BaseWorkflowViewModel
import com.code.assignment.ui.listener.BusyIndicatorRequestListener
import com.code.assignment.ui.listener.MessageRequestListener
import com.code.assignment.ui.mapper.UIModelMapperFactory
import com.code.assignment.ui.screen.feed.model.UIHeroNewsItem
import com.code.assignment.ui.screen.feed.model.UINewsFeedModel
import com.code.assignment.ui.screen.feed.model.UINewsItem
import com.code.assignment.ui.uicomponents.model.UIError
import com.code.assignment.ui.uicomponents.model.UIItem
import com.code.assignment.ui.uicomponents.model.UIModel
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

class NewsFeedViewModel @Inject constructor(
    private val fetchNewsFeedUseCase: FetchNewsFeedUseCase,
    private val uiModelMapperFactory: UIModelMapperFactory
) : BaseWorkflowViewModel<NewsFeedViewModel.Listener>() {

    private val _modelLiveData: MutableLiveData<UINewsFeedModel?> = MutableLiveData()
    val modelLiveData = _modelLiveData as LiveData<UINewsFeedModel?>

    override fun onCleared() {
        fetchNewsFeedUseCase.dispose()
        super.onCleared()
    }

    override fun onMasterCommListenerSet() {
    }

    fun fetchNewsFeed() {
        masterListener?.toggleBusyIndicator(true)
        fetchNewsFeedUseCase.execute(object : DisposableObserver<SResult<FeedResponse>>() {
            override fun onComplete() {
                //Do nothing
            }

            override fun onNext(t: SResult<FeedResponse>) {
                val model =
                    uiModelMapperFactory.create<UINewsFeedModel, SResult<FeedResponse>>(
                        UINewsFeedModel::class.java
                    ).fromDomain(t)
                when (model) {
                    is UIModel.Success -> {
                        _modelLiveData.postValue(model.data)
                    }
                    is UIModel.Error -> masterListener?.showToast(model.error)
                }
                masterListener?.toggleBusyIndicator(false)
            }

            override fun onError(e: Throwable) {
                masterListener?.showToast(UIError())
                masterListener?.toggleBusyIndicator(false)
            }

        }, null)
    }

    fun onItemClicked(item: UIItem) {
        when (item) {
            is UINewsItem,
            is UIHeroNewsItem -> masterListener?.onNewsDetailsRequested(item as UINewsItem)
            /*
                Other items shall be handled in future, as required.
             */
            else -> masterListener?.showToast(UIError())
        }
    }

    fun onRefreshRequested() {
        fetchNewsFeed()
    }

    /**
     * The interface which enables communication with the
     * master/controller
     */
    interface Listener : BusyIndicatorRequestListener, MessageRequestListener {

        fun onNewsDetailsRequested(item: UINewsItem)

    }
}