package com.code.assignment.ui.uicomponents.adapter

import androidx.recyclerview.widget.RecyclerView
import com.code.assignment.ui.uicomponent.listener.OnItemViewClickListener
import com.code.assignment.ui.uicomponents.model.UIItem
import com.code.assignment.ui.uicomponents.viewholder.SItemViewHolder

abstract class SAdapter<ON_CLICK_LISTENER : OnItemViewClickListener<UIItem>>(
    protected val listener: ON_CLICK_LISTENER
) : RecyclerView.Adapter<SItemViewHolder<*, UIItem, OnItemViewClickListener<UIItem>>>() {

    private val items: MutableList<UIItem> by lazy {
        mutableListOf<UIItem>()
    }

    fun setData(items: List<UIItem>) {
        this.items.clear()
        this.items.addAll(items)

        notifyDataSetChanged()
    }

    final override fun getItemViewType(position: Int): Int {
        return if (position < items.size) {
            val uiItem = items[position]

            uiItem.itemViewType

        } else {
            super.getItemViewType(position)
        }
    }

    final override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(
        holder: SItemViewHolder<*, UIItem, OnItemViewClickListener<UIItem>>,
        position: Int
    ) {
        /*
            This check is to take care of the possibility that the list is being modified while
            onBindViewHolder is called - an edge case but still needs to be handled.
         */
        if (position < items.size) {
            val item = items[position]
            holder.bind(item, position)
        }
    }
}