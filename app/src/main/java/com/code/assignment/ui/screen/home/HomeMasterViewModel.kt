package com.code.assignment.ui.screen.home

import androidx.lifecycle.ViewModel
import au.com.abc.news.R
import com.code.assignment.ui.base.SingleLiveEvent
import com.code.assignment.ui.screen.details.NewsItemDetailsViewModel
import com.code.assignment.ui.screen.feed.NewsFeedViewModel
import com.code.assignment.ui.screen.feed.model.UINewsItem
import com.code.assignment.ui.uicomponents.model.UIError
import javax.inject.Inject

/**
 * [ViewModel] which manages all the view related data that is to be fed into [HomeActivity]
 *
 * There is no reference to any View and/or Activity instance in this class.
 * Data is provided to the "UI"/view via LiveData objects.
 *
 * Each LiveData object controls the smallest meaningful view component within the entire
 * Activity layout, as required.
 *
 */
class HomeMasterViewModel @Inject constructor() : ViewModel(),
    NewsFeedViewModel.Listener,
    NewsItemDetailsViewModel.Listener {

    val fragmentMovementLiveData =
        SingleLiveEvent<UIFragmentModel<HomeMasterViewModel>?>()

    val errorToastLiveData =
        SingleLiveEvent<UIError?>()
    val progressVisibilityLiveData =
        SingleLiveEvent<Boolean?>()
    val popBackStackLiveData =
        SingleLiveEvent<Unit?>()

    /*
        The data is being shared between the Fragments via the
        Master View Model.
        Since data persistence is not mandatory for the assignment,
        this approach still helps to make sure the data is persisted
        during configuration changes.
     */
    private var selected: UINewsItem? = null


    fun init() {
        fragmentMovementLiveData.postValue(
            UIFragmentModel(
                fragmentType = FragmentType.NEWS_FEED,
                masterListenerClass = HomeMasterViewModel::class.java
            )
        )
    }

    override fun onNewsDetailsRequested(item: UINewsItem) {
        selected = item
        fragmentMovementLiveData.postValue(
            UIFragmentModel(
                fragmentType = FragmentType.ITEM_DETAILS,
                masterListenerClass = HomeMasterViewModel::class.java,
                addToBackStack = true,
                animation = UIAnimation(
                    enter = R.anim.slide_in_right,
                    exit = 0,
                    popEnter = 0,
                    popExit = R.anim.slide_out_right
                )
            )
        )
    }

    override fun toggleBusyIndicator(show: Boolean) {
        progressVisibilityLiveData.postValue(show)
    }

    override fun showToast(uiError: UIError?) {
        errorToastLiveData.postValue(uiError)
    }

    fun onBackPressed() {
        if (progressVisibilityLiveData.value == true) {
            progressVisibilityLiveData.postValue(false)
        }
    }

    override fun getNewsItemDetails(): UINewsItem? {
        return this.selected
    }
}