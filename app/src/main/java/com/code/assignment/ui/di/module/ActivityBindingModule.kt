package com.code.assignment.ui.di.module

import com.code.assignment.ui.screen.home.HomeActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    internal abstract fun bindHomeActivity(): HomeActivity

}