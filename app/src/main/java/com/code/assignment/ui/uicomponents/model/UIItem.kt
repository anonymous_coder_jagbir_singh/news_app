package com.code.assignment.ui.uicomponents.model

interface UIItem {

    val id: Int

    val itemViewType: Int

}