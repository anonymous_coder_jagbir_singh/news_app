package com.code.assignment.ui.screen.feed.model

import com.code.assignment.ui.uicomponents.model.UIItem
import java.util.*

/**
 * Class that contains the data that is required for each News Item
 */
open class UINewsItem(
    val title: String,
    val imageUrl: String,
    val date: Date,
    val content: String,
    override val id: Int
) : UIItem {

    companion object {
        val ITEM_VIEW_TYPE = Objects.hash("UINewsItem")
    }

    override val itemViewType: Int
        get() = ITEM_VIEW_TYPE

}