package com.code.assignment.ui.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.code.assignment.ui.di.SViewModelFactory
import com.code.assignment.ui.di.annotation.ViewModelKey
import com.code.assignment.ui.screen.details.NewsItemDetailsViewModel
import com.code.assignment.ui.screen.feed.NewsFeedViewModel
import com.code.assignment.ui.screen.home.HomeMasterViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(NewsItemDetailsViewModel::class)
    internal abstract fun bindNewsItemDetailsViewModel(viewModel: NewsItemDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeMasterViewModel::class)
    internal abstract fun bindHomeMasterViewModel(viewModel: HomeMasterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewsFeedViewModel::class)
    internal abstract fun bindNewsFeedViewModel(viewModel: NewsFeedViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: SViewModelFactory): ViewModelProvider.Factory


}