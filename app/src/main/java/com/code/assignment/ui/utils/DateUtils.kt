package com.code.assignment.ui.utils

import android.text.Spannable
import androidx.annotation.ColorInt
import java.text.SimpleDateFormat
import java.util.*

/**
 * Extension to convert [Date] into a string
 * of the following format:
 *
 * MMM d, yyyy HH:mm a
 *
 * Example: Mar 16, 2019 12:33 PM
 */
fun Date.`toStringMMM-d-YYYY-HH-mm-a`(): String {
    return SimpleDateFormat("MMM d, yyyy hh:mm a", Locale.getDefault()).format(this)
}

fun Date.`toSpannableMMM-d-YYYY-HH-mm-a`(@ColorInt color: Int): Spannable {
    val coloredSubText = SimpleDateFormat("MMM d, yyyy", Locale.getDefault()).format(this)
    val fullText = this.`toStringMMM-d-YYYY-HH-mm-a`()
    return TextFormatUtils.getColoredText(fullText, color, coloredSubText)
}