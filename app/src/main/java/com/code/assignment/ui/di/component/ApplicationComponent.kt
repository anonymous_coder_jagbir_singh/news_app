package com.code.assignment.ui.di.component

import com.code.assignment.ui.base.SApplication
import com.code.assignment.ui.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        FragmentBindingModule::class,
        DataModule::class,
        DomainModule::class,
        ViewModelModule::class,
        AppModule::class]
)
interface ApplicationComponent : AndroidInjector<SApplication> {

    override fun inject(application: SApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: SApplication): Builder

        fun build(): ApplicationComponent
    }
}