package com.code.assignment.ui.mapper

import com.code.assignment.domain.carrier.SResult
import com.code.assignment.ui.uicomponents.model.UIError
import com.code.assignment.ui.uicomponents.model.UIModel

/**
 *
 * Base UI Mapper implementation to handle all common mappings
 * from DOMAIN layer.
 * And also to handle common error scenarios.
 *
 * Based on the current set of requirements,
 * it is assumed that all the UI Models shall conform to [UIModel]
 * with ERROR always being of type [UIError].
 *
 * [UIMapper] can be directly implemented for models containing more complex
 * data structures for UI_MODEL and ERROR components
 */
abstract class UIBaseMapper<UI_MODEL, DOMAIN_MODEL>
    : UIMapper<UIModel<UI_MODEL>, SResult<DOMAIN_MODEL>> {

    override fun fromDomain(domain: SResult<DOMAIN_MODEL>): UIModel<UI_MODEL> {
        return when (domain) {
            is SResult.Error -> {
                UIModel.Error<UI_MODEL>(error = UIError(domain.error.message))
            }
            is SResult.Success -> {
                getModel(domain.data)
            }
        }
    }

    protected abstract fun getModel(domainModel: DOMAIN_MODEL): UIModel<UI_MODEL>

}