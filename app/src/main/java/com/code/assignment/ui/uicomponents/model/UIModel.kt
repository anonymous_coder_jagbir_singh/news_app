package com.code.assignment.ui.uicomponents.model

/**
 * Based on the current set of requirements,
 * it is assumed that all the UI Models shall always
 * have ERROR of type [UIError]
 *
 * But in case there is a need to provide more complex errors,
 * the ERROR can also be made generic:
 * UIModel<DATA, ERROR: UIError>
 *
 */
sealed class UIModel<T> {

    class Success<T>(val data: T) : UIModel<T>()

    class Error<T>(val error: UIError) : UIModel<T>()

}