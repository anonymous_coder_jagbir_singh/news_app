package com.code.assignment.ui.listener

/**
 * The interface that provides updates when
 * the busy indicator (progress bar) needs
 * to be toggled.
 *
 * Each feature/function has been encapsulated within
 * an interface so that they can be chosen as needed,
 * without the worrying of having an exposed method,
 * which might never be used in a particular scenario.
 *
 * Example:
 * ScreenType A needs - progress bar, toast
 * ScreenType B needs - toast, keyboard
 *
 * Thus, ScreenType A would use only progress bar and toast related
 * listener interfaces.
 * And ScreenType B would use only toast and keyboard related
 * listener interfaces.
 *
 * This helps in making sure that each entity/consumer has access
 * to only those features/functions which it understands and needs
 * rather than exposing redundant features/functions to it.
 *
 */
interface UpLevelRequestListener {

    fun onUpButtonClicked()

}