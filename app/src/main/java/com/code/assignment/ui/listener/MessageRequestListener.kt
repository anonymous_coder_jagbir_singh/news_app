package com.code.assignment.ui.listener

import com.code.assignment.ui.uicomponents.model.UIError


/**
 * The interface that provides updates when
 * messaging needs to be shown - currently
 * handled/implemented as a toast.
 *
 * Each feature/function has been encapsulated within
 * an interface so that they can be chosen as needed,
 * without the worrying of having an exposed method,
 * which might never be used in a particular scenario.
 *
 * Example:
 * Screen A needs - progress bar, toast
 * Screen B needs - toast, keyboard
 *
 * Thus, Screen A would use only progress bar and toast related
 * listener interfaces.
 * And Screen B would use only toast and keyboard related
 * listener interfaces.
 *
 * This helps in making sure that each entity/consumer has access
 * to only those features/functions which it understands and needs
 * rather than exposing redundant features/functions to it.
 *
 */
interface MessageRequestListener {

    fun showToast(uiError: UIError?)

}