package com.code.assignment.ui.screen.feed.mapper

import com.code.assignment.domain.usecase.news.model.FeedResponse
import com.code.assignment.ui.mapper.UIBaseMapper
import com.code.assignment.ui.screen.feed.model.UIHeroNewsItem
import com.code.assignment.ui.screen.feed.model.UINewsFeedModel
import com.code.assignment.ui.screen.feed.model.UINewsItem
import com.code.assignment.ui.uicomponents.model.UIError
import com.code.assignment.ui.uicomponents.model.UIModel

class UINewsFeedMapper() :
    UIBaseMapper<UINewsFeedModel, FeedResponse>() {

    override fun getModel(domain: FeedResponse): UIModel<UINewsFeedModel> {
        return if (domain.status == FeedResponse.STATUS_OK) { //check if status is ok
            val uiItems = mutableListOf<UINewsItem>()
            for (index in domain.items.indices) {
                val uiItem: UINewsItem
                val item = domain.items[index]
                uiItem = if (index == 0) {
                    UIHeroNewsItem(
                        item.title,
                        item.enclosure.link,
                        item.pubDate,
                        item.content,
                        index
                    )
                } else {
                    UINewsItem(
                        item.title,
                        item.thumbnail,
                        item.pubDate,
                        item.content,
                        index
                    )
                }
                uiItems.add(uiItem)
            }
            UIModel.Success(UINewsFeedModel(domain.feed.title, uiItems))

        } else {
            //This is for a default unknown error message handling
            UIModel.Error(UIError())
        }
    }
}