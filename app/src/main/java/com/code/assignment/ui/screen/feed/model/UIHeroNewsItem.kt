package com.code.assignment.ui.screen.feed.model

import java.util.*

/**
 * Class that contains the data that is required for each Hero News Item.
 * This is an extension of [UINewsItem] since the data contained is the
 * same but it identified as a separate view type.
 */
class UIHeroNewsItem(
    title: String,
    imageUrl: String,
    date: Date,
    content: String,
    override val id: Int
) : UINewsItem(title, imageUrl, date, content, id) {

    companion object {
        val ITEM_VIEW_TYPE = Objects.hash("UIHeroNewsItem")
    }

    override val itemViewType: Int
        get() = ITEM_VIEW_TYPE

}