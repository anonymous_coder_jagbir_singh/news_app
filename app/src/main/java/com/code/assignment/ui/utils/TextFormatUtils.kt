package com.code.assignment.ui.utils

import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import androidx.annotation.ColorInt

object TextFormatUtils {
    fun getColoredText(
        text: String,
        @ColorInt color: Int,
        vararg coloredSubText: String
    ): Spannable {
        val spannable = SpannableString(text)

        for (textToColor in coloredSubText) {
            val start = text.indexOf(textToColor)
            if (start > -1) {
                spannable.setSpan(
                    ForegroundColorSpan(color),
                    start,
                    start + textToColor.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        }
        return spannable
    }
}