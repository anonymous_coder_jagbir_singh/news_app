package com.code.assignment.ui.screen.home

import androidx.annotation.AnimRes

class UIAnimation(
    @AnimRes val enter: Int = 0,
    @AnimRes val exit: Int = 0,
    @AnimRes val popEnter: Int = 0,
    @AnimRes val popExit: Int = 0
)