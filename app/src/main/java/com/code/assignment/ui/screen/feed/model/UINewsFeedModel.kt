package com.code.assignment.ui.screen.feed.model

import com.code.assignment.ui.uicomponents.model.UIItem

class UINewsFeedModel(
    val title: String,
    val items: List<UIItem>
)